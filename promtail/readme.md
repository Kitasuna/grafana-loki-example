# Надстройка экспортера логов в виде json докера

Включение экспортера:
https://docs.docker.com/config/containers/logging/json-file/


To use the json-file driver as the default logging driver, set the log-driver and log-opts keys to appropriate values in the daemon.json file, which is located in /etc/docker/ on Linux hosts or C:\ProgramData\docker\config\ on Windows Server. If the file does not exist, create it first. For more information about configuring Docker using daemon.json, see daemon.json.

The following example sets the log driver to json-file and sets the max-size and max-file options to enable automatic log-rotation.

```json
{
"log-driver": "json-file",
"log-opts": {
    "max-size": "10m",
    "max-file": "3",
    "tag": "{{.ImageName}}/{{.Name}}/{{.ID}}"
  }
}
```

Возможные лог "tag":
https://docs.docker.com/config/containers/logging/log_tags/

Альтернатива:
https://grafana.com/docs/loki/latest/send-data/docker-driver/configuration/